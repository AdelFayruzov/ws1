from django.http import HttpResponse, JsonResponse
import json
from django.contrib.auth.models import User
from django import forms
from .forms import *
from .models import Product, Basket, Order
from django.contrib import auth
from rest_framework.authtoken.models import Token


def error404(request, exception=None):
    return HttpResponse(json.dumps({"error": {"code": 404, "message": "Not found"}}), content_type="application/json",
                        status=404)


def signup(request):
    if request.method == 'POST':
        data = (json.load(request))
        username = data['fio']
        email = data['email']
        password = data['password']
        if not User.objects.filter(email=email, username=username).exists():
            user = User.objects.create_user(username, email, password)
            if user is not None:
                token = Token.objects.create(user=user)
                return HttpResponse(json.dumps({"data": {"user_token": str(token.key)}}),
                                    content_type="application/json", status=200)
            else:
                return HttpResponse('Validation error', content_type="application/json", status=403)
        else:
            return HttpResponse('Validation error', content_type="application/json", status=403)


def create_product(request):
    if request.user.is_authenticated:
        if request.user.is_staff:
            if request.method == 'POST':
                data = (json.load(request))
                title = data['title']
                description = data['description']
                price = data['price']
                new_product = Product(title=title, description=description, price=price)
                if new_product is not None:
                    new_product.save()
                    return HttpResponse(json.dumps({"data": {"id": new_product.id, "message": "Product added"}}),
                                        content_type="application/json", status=201)
                else:
                    return HttpResponse('Error', content_type="application/json", status=403)
        else:
            return forbidden_for_you()
    else:
        return login_error()


def login_request(request):
    if request.method == 'POST':
        data = (json.load(request))
        email = data['email']
        password = data['password']
        try:
            username = User.objects.get(email=email)
        except:
            return HttpResponse(json.dumps({"error": {"code": 401, "message": "Authentication failed"}}),
                                content_type="application/json", status=401)
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            token = Token.objects.get(user=user)
            return HttpResponse(json.dumps({"data": {"user_token": str(token.key)}}), content_type="application/json",
                                status=200)
        else:
            return HttpResponse(json.dumps({"error": {"code": 401, "message": "Authentication failed"}}),
                                content_type="application/json", status=401)


def change_product_or_delete(request, *args, **kwargs):
    if request.user.is_authenticated:
        if request.user.is_staff:
            if request.method == 'PATCH':
                # if request.user.is_authenticated:
                data = (json.load(request))
                title = data['title']
                description = data['description']
                price = data['price']
                pk = kwargs.get("pk", None)
                product = Product.objects.get(id=pk)
                product.title = title
                product.description = description
                product.price = price
                product.save()
                return HttpResponse(json.dumps({"data": {"id": product.id, "title": product.title,
                                                         "description": product.description, "price": product.price}}),
                                    content_type="application/json", status=200)
                # else:
                #     return HttpResponse('You are not autorization', content_type = "application/json", status=403)
            if request.method == 'DELETE':
                pk = kwargs.get("pk", None)
                product = Product.objects.get(id=pk)
                product.delete()
                return HttpResponse(json.dumps({"data": {"message": "Product removed"}}),
                                    content_type="application/json", status=200)
        else:
            return forbidden_for_you()
    else:
        return login_error()


def logout_request(request):
    if request.method == 'GET':
        auth.logout(request)
        return HttpResponse(json.dumps({"data": {"message": "logout"}}), content_type="application/json", status=200)


def show_products(request):
    if request.method == 'GET':
        products = Product.objects.all()
        list_products = []
        for i in products:
            list_products.append(dict(id=i.id, name=i.title, description=i.description, price=i.price))
        return HttpResponse(json.dumps({"data": list_products}), content_type="application/json", status=200)


def add_to_basket_or_delete(request, *args, **kwargs):
    if request.method == 'POST':
        pk = kwargs.get("pk", None)
        product = Product.objects.get(id=pk)
        new_product_in_basket = Basket(product_id=product.id, title=product.title, description=product.description,
                                       price=product.price)
        new_product_in_basket.save()
        return HttpResponse(json.dumps({"data": {"message": "Product add to card"}}), content_type="application/json",
                            status=201)

    elif request.method == 'DELETE':
        pk = kwargs.get("pk", None)
        product = Basket.objects.get(id=pk)
        product.delete()
        return HttpResponse(json.dumps({"data": {"message": "Item removed from basket"}}),
                            content_type="application/json", status=200)


def show_products_in_basket(request):
    if request.method == 'GET':
        basket = Basket.objects.all()
        list_products = []
        for product in basket:
            list_products.append(
                dict(id=product.id, product_id=product.product_id, title=product.title, description=product.description,
                     price=product.price))
        return HttpResponse(json.dumps({"data": list_products}), content_type="application/json", status=200)


def create_order(request):
    if request.method == 'POST':
        products = []
        order_price = 0
        basket = Basket.objects.all()
        for product in basket:
            products.append(product.product_id)
            order_price += product.price
        order = Order(products=products, order_price=order_price)
        order.save()
        basket.delete()
        return HttpResponse(json.dumps({"data": {"order_id": order.id, "message": "Order is processed"}}),
                            content_type="application/json", status=201)
    elif request.method == 'GET':
        orders = Order.objects.all()
        list_orders = []
        for order in orders:
            list_orders.append(dict(id=order.id, products=order.products, order_price=order.order_price))
        return HttpResponse(json.dumps({"data": list_orders}), content_type="application/json", status=200)


def login_error():
    return HttpResponse(json.dumps({"error": {"code": 403, "message": "Login failed"}}),
                        content_type="application/json", status=403)


def forbidden_for_you():
    return HttpResponse(json.dumps({"error": {"code": 403, "message": "Forbidden for you"}}),
                        content_type="application/json", status=403)
