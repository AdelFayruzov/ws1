from rest_framework import routers
from products.api import ProductAPIDetailView, ProductAPIUpdate, ProductViewList
from django.urls import path, include
from .views import *

# router = routers.SimpleRouter()
# router.register(r'product', ProductViewSet)
# router.register('api/registration', NewUserFormViewSet, 'registration')


# urlpatterns = router.urls

urlpatterns = [
    # path('api/products/', ProductViewList.as_view()),
    # path('api/auth/', include('rest_framework.urls')),
    # path('api/product/<int:pk>/', ProductAPIUpdate.as_view()),
    # path('api/productdelete/<int:pk>/', ProductAPIDetailView.as_view()),
    path('api/signup/', signup),
    path('api/login/', login_request),
    path('api/product/', create_product),
    path('api/products/', show_products),
    path('api/product/<int:pk>/', change_product_or_delete),
    path('api/cart/<int:pk>/', add_to_basket_or_delete),
    path('api/cart/', show_products_in_basket),
    path('api/order/', create_order),
    path('api/logout/', logout_request),
    # path('api/', include(router.urls)),
]
