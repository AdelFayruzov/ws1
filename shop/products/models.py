from pyexpat import model

from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


# Create your models here.

class Product(models.Model):
    title = models.CharField(max_length=15)
    description = models.TextField(max_length=100)
    price = models.IntegerField()

    # def __str__(self):
    #     return self.title


# class Basket(models.Model):
#     product = models.ForeignKey(Product, verbose_name="Product", on_delete=models.CASCADE)

class Basket(models.Model):
    owner = models.ForeignKey('Customer', null=True, verbose_name='Владелец', on_delete=models.CASCADE)
    id = models.IntegerField(primary_key=True, default=None)
    product_id = models.IntegerField()
    title = models.CharField(max_length=15)
    description = models.TextField(max_length=100)
    price = models.IntegerField()


class Customer(models.Model):
    user = models.OneToOneField(User, verbose_name='Пользователь', on_delete=models.CASCADE, related_name='customer')


class Order(models.Model):
    # id = models.IntegerField(primary_key=True)
    products = models.TextField()
    order_price = models.IntegerField()
