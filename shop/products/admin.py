from django.contrib import admin
from .models import *


# Register your models here.

class ProductAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'price')


class BasketAdmin(admin.ModelAdmin):
    list_display = ('product_id', 'title', 'description', 'price')


class OrdertAdmin(admin.ModelAdmin):
    list_display = ('products', 'order_price')


admin.site.register(Product, ProductAdmin)
admin.site.register(Basket, BasketAdmin)
admin.site.register(Order, OrdertAdmin)
